# -*- coding: utf-8 -*-

# Define your item pipelines here
#
# Don't forget to add your pipeline to the ITEM_PIPELINES setting
# See: http://doc.scrapy.org/en/latest/topics/item-pipeline.html

from scrapy.exceptions import DropItem
import re
from arango import ArangoClient
import datetime
from termcolor import colored


class PostprocessPipeline(object):

    lat_begin = None
    lat_end = None
    lon_begin = None
    lon_end = None

    def __init__(self, lat_begin, lat_end, lon_begin, lon_end):
        self.lat_begin = lat_begin
        self.lat_end = lat_end
        self.lon_begin = lon_begin
        self.lon_end = lon_end

    @classmethod
    def from_crawler(cls, crawler):
        return cls(
            lat_begin = crawler.settings.get('LATITUDE_BEGIN'),
            lon_begin=crawler.settings.get('LONGITUDE_BEGIN'),
            lat_end=crawler.settings.get('LATITUDE_END'),
            lon_end=crawler.settings.get('LONGITUDE_END'),
        )

    def item_type(self, t):
        # Choose house type
        t = t.lower()
        if ( t.find("apart") != -1 ):
            return "apartment"
        elif ( t.find("bunga")!=-1 ):
            return "bungalow"
        elif (t.find("duplex") != -1):
            return "duplex"
        elif ( t.find("site")!=-1 ):
            return "site"
        elif ( t.find("house")!=-1 ):
            if ( t.find("terra")!=-1 ):
                return "terrace_house"
            elif ( t.find("semi")!=-1 ):
                return "semidetach_house"
            elif ( t.find("detach")!=-1 ):
                return "detach_house"
            elif (t.find("townhouse") != -1):
                return "town_house"
            else:
                return "house"
        elif ( t.find("student")!=-1):
            return "student"
        else:
            return None

    def item_beds(self, beds):
        # Count beds
        if ( beds != None ):
            digit = re.search("\d+(?:\.\d{2})?", beds)
            if (digit):
                return int(digit.group())
        return None

    def item_price(self, price):
        if( price != None ):
            try:
                price = price.lower()
                pstr = price.replace(',','')
                pstr = pstr.replace('€', '')
                d = pstr
                #Check if this is rent price
                rent = re.search("week", pstr)
                if ( rent ):
                    digit = re.search("\d+(?:\.\d{2})?", pstr)
                    d = digit.group()
                    d = int(d)*4
                rent = re.search("month", pstr)
                if ( rent ):
                    digit = re.search("\d+(?:\.\d{2})?", pstr)
                    d = digit.group()
                return int(d)
            except ValueError:
                return None
            except IndexError:
                return None
        return None

    def item_name(self, name):
        s = name.replace('\n','')
        return s.strip()

    def process_item(self, item, spider):
        item["price"] = self.item_price(item["price"])
        item["type"] = self.item_type(item["type"])
        item["beds"] = self.item_beds(item["beds"])
        item["name"] = self.item_name(item["name"])

        # Check if floor was specified in sq. foot instead of metres only for sales
        if (item["sale_rent"]==1 and item["floor"]):
            sqm_price = item["price"]/item["floor"]
            if ( sqm_price < 1500 and item["beds"]<5 and item["floor"]>500 ):
                print()
                print(colored('Converting square foot to square meters for "' + item["url"] +'"','yellow',attrs=['bold']))
                item["floor"] = int(item["floor"] * 0.092903044)
        elif (item["sale_rent"]==0 and item["beds"]):
            bedprice = item['price'] / item['beds']
            # If this is just single room for rent then fix it automatically
            if (bedprice < 350):
                print()
                print(colored('This is a room for rent in "' + item["url"] +'"','yellow',attrs=['bold']))
                item['beds'] = 1
                item['size'] = 'small'

        #Check Dublin bounds
        if ( item["lat"] < self.lat_begin or item["lat"] > self.lat_end ):
            raise DropItem("Latitude is outside of area in %s" % item)
        if ( item["lon"] < self.lon_begin or item["lon"] > self.lon_end ):
            raise DropItem("Longitude is outside of area in %s" % item)
        #Check proper size
        if ( item["type"]=="site" ):
            raise DropItem("Ignoring empty site %s" % item)
        if ( item["beds"] or item["floor"] ):
            if (item["price"]):
                return item
            else:
                raise DropItem("Missing price in %s" % item)
        else:
            raise DropItem("Missing size in %s" % item)


class DBPipeline(object):

    client = None
    price_col = None
    price200m_col = None
    rent_col = None
    rent200m_col = None
    property_sale_col = None
    property_rent_col = None
    db = None

    def __init__(self, db_uri, db_name):
        self.client = ArangoClient(username='root',password='F33dback34',host=db_uri)
        self.db = self.client.db(db_name)
        self.price_col = self.db.collection('price')
        self.price200m_col = self.db.collection('price_200m')
        self.rent_col = self.db.collection('rent')
        self.rent200m_col = self.db.collection('rent_200m')
        self.property_sale_col = self.db.collection('property_sale')
        self.property_rent_col = self.db.collection('property_rent')

    @classmethod
    def from_crawler(cls, crawler):
        return cls(
            db_uri = crawler.settings.get('DB_URI'),
            db_name = crawler.settings.get('DB_NAME')
        )

    def process_item(self, item, spider):
        # Save area with 100m resolution
        query = 'FOR doc IN NEAR("grid",' + str(item["lat"]) + ',' + str(item["lon"]) + ',25) RETURN doc'
        cursor = self.db.aql.execute(query)
        docs = cursor.batch()
        for doc in docs:
            price_ref = doc.get("price_ref")
            today = datetime.date.today()
            price = {'reference': price_ref, 'size': item.size, 'price': item["price"], 'town': item["town"], 'date': today.isoformat() }
            if( item["sale_rent"]==1 ):
                r = self.price_col.insert(price)
            else:
                r = self.rent_col.insert(price)
        # Save area with 200m resolution
        query = 'FOR doc IN NEAR("grid_200m",' + str(item["lat"]) + ',' + str(item["lon"]) + ',12) RETURN doc'
        cursor = self.db.aql.execute(query)
        docs = cursor.batch()
        for doc in docs:
            price_ref = doc.get("price_ref")
            today = datetime.date.today()
            price = {'reference': price_ref, 'size': item.size, 'price': item["price"], 'town': item["town"], 'date': today.isoformat() }
            if( item["sale_rent"]==1 ):
                r = self.price200m_col.insert(price)
            else:
                r = self.rent200m_col.insert(price)
        # Save property in prop list
        prop = {'name': item["name"], 'latitude': item["lat"], 'longitide': item["lon"], 'size': item.size,
                'price': item["price"], 'type': item["type"], 'beds': item["beds"], 'floor': item["floor"],
                'uri': item["url"], 'date': today.isoformat(), 'town': item["town"]}
        if ( item["sale_rent"]==1) :
            r = self.property_sale_col.insert(prop)
        else:
            r = self.property_rent_col.insert(prop)
        return item


class CSVPipeline(object):

    file = None

    def __init__(self, sale_file):
        self.file = open(sale_file, 'wt')

    def __del__(self):
        self.file.flush()
        self.file.close()

    @classmethod
    def from_crawler(cls, crawler):
        return cls(
            sale_file = crawler.settings.get('SALE_FILE')
        )

    def process_item(self, item, spider):
        today = datetime.date.today()
        s = today.isoformat() + '|' + item["town"] + '|' + str(item["lat"]) + '|' + str(item["lon"]) + '|' + item["name"] \
            + '|' + item.size + '|' + str(item["price"]) + '|' + item["type"] + '|' + str(item["beds"]) + '|' \
            + str(item["floor"]) + '|' + str(item["sale_rent"]) + '|' + item["url"] + '\n'
        self.file.write(s)
        return item
