# -*- coding: utf-8 -*-

# Define here the models for your scraped items
#
# See documentation in:
# http://doc.scrapy.org/en/latest/topics/items.html

import scrapy

class DaftItem(scrapy.Item):
    # define the fields for your item here like:
    # name = scrapy.Field()
    url = scrapy.Field()
    name = scrapy.Field()
    price = scrapy.Field()
    type = scrapy.Field()
    beds = scrapy.Field()
    floor = scrapy.Field()
    lat = scrapy.Field()
    lon = scrapy.Field()
    sale_rent = scrapy.Field()
    town = scrapy.Field()

    @property
    def size(self):
        #If there is a floor size
        if (self["floor"]):
            if (self["floor"]<80):
                return "small"
            elif (self["floor"]<130):
                return "medium"
            elif (self["floor"] < 180):
                return "large"
            else:
                return "huge"
        #otherwise decide on bedrooms
        if (self["beds"] == 1):
            return "small"
        elif (self["beds"] == 2):
            return "small"
        elif (self["beds"] == 3):
            return "medium"
        elif (self["beds"] == 4 or self["beds"] == 5):
            return "large"
        elif (self["beds"] > 5):
            return "huge"
        else:
            return None

