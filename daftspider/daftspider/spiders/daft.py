#!/usr/bin/python3
# -*- coding: utf-8 -*-
import scrapy
import re
from daftspider.items import DaftItem
from termcolor import colored


class DaftSpider(scrapy.Spider):

    name = "daft"
    allowed_domains = ["daft.ie"]
    start_urls = [ 'http://www.daft.ie/dublin', ]


    def parse(self, response):
        for sel in response.xpath('//body/div[@id="content"]/div[@id="gc_content"]/div[@class="list_holder on_county_page"]/ul/li'):
            title = sel.xpath('a/text()').extract()
            link = sel.xpath('a/@href').extract()
            desc = sel.xpath('text()').extract()
            # Choose only properties for sale
            if ( link[0].find('property-for-sale')!=-1 ):
                town = {'name': title[0], 'latitude': 0, 'longitide': 0}
                url = response.urljoin(link[0])
                request = scrapy.Request(url, callback=self.parse_sale_area)
                request.meta['title'] = title[0]
                yield request
            # Look for rent price
            elif ( link[0].find('property-for-rent')!=-1 ):
                #print(title, link, desc)
                url = response.urljoin(link[0])
                request = scrapy.Request(url, callback=self.parse_rent_area)
                request.meta['title'] = title[0]
                yield request


    def parse_sale_area(self, response):
        url = ''
        for sel in response.xpath('//body/table[@id="sr_content"]/tr/td/div[@class="box"]/div[@class="search_result_title_box"]/h2'):
            link = sel.xpath('a/@href').extract()
            url = response.urljoin(link[0])
            #Make sure we are in Dublin area
            dub = re.search('dublin', url)
            if ( not dub ):
                print('Go outside the greater Dublin area, prop: ', url)
                continue
            request = scrapy.Request(url, callback=self.parse_property)
            item = DaftItem()
            item["sale_rent"] = 1
            item["town"] = response.meta['title']
            request.meta['item'] = item
            yield request
        #Look if there is a next page
        sel = response.xpath('//body/table[@id="sr_content"]/tr/td/ul[@class="paging clear"]/li[@class="next_page"]')
        if ( len(sel)>0 ):
            link = sel.xpath('a/@href').extract()
            url1 = url
            url = response.urljoin(link[0])
            dub = re.search('dublin', url)
            if ( not dub ):
                print('Go outside the greater Dublin area, page: ', url)
                return
            yield scrapy.Request(url, callback=self.parse_sale_area)


    def parse_rent_area(self, response):
        for sel in response.xpath('//body/table[@id="sr_content"]/tr/td/div[@class="box"]/div[@class="search_result_title_box"]/h2'):
            link = sel.xpath('a/@href').extract()
            url = response.urljoin(link[0])
            #Make sure we are in Dublin area
            dub = re.search('dublin', url)
            if ( not dub ):
                print('Go outside the greater Dublin area, prop: ', url)
                continue
            request =  scrapy.Request(url, callback=self.parse_property)
            item = DaftItem()
            item["sale_rent"] = 0
            item["town"] = response.meta['title']
            request.meta['item'] = item
            yield request
        #Look if there are next page
        sel = response.xpath('//body/table[@id="sr_content"]/tr/td/ul[@class="paging clear"]/li[@class="next_page"]')
        if ( len(sel)>0 ):
            link = sel.xpath('a/@href').extract()
            url = response.urljoin(link[0])
            dub = re.search('dublin', url)
            if ( not dub ):
                print('Go outside the greater Dublin area, page: ', url)
                return
            yield scrapy.Request(url, callback=self.parse_rent_area)


    def parse_property(self, response):
        item = response.meta['item']
        type = ""
        bed = ""
        floor = None
        desc = response.xpath('//body/div[@id="content"]/div[@class="smi-twocolumns"]/div[@id="smi-content"]/div[@class="smi-info"]/div[@class="smi-holder"]/div[@class="smi-object-info"]/div[@class="smi-object-header"]/h1/text()').extract()
        price = response.xpath('//body/div[@id="content"]/div[@class="smi-twocolumns"]/div[@id="smi-content"]/div[@class="smi-info"]/div[@class="smi-holder"]/div[@class="smi-object-info"]/div[@id="smi-summary-items"]/div[@id="smi-price-string"]/text()').extract()
        if (len(price)==0):
            price = response.xpath('//body/div[@id="content"]/div[@class="smi-twocolumns"]/div[@id="smi-content"]/div[@class="smi-info"]/div[@class="smi-holder"]/div[@class="smi-object-info"]/div[@id="smi-summary-items"]/span[@id="pch-latest"]/div[@class="price-changes-sr"]/span[@class="price-change-down"]/text()').extract()
        if (len(price)==0):
            price = response.xpath('//body/div[@id="content"]/div[@class="smi-twocolumns"]/div[@id="smi-content"]/div[@class="smi-info"]/div[@class="smi-holder"]/div[@class="smi-object-info"]/div[@id="smi-summary-items"]/span[@id="pch-latest"]/div[@class="price-changes-sr"]/span[@class="price-change-down"]/text()').extract()
        if (len(price)>0):
            price = price[0]
        for sel in response.xpath('//body/div[@id="content"]/div[@class="smi-twocolumns"]/div[@id="smi-content"]/div[@class="smi-info"]/div[@class="smi-holder"]/div[@class="smi-object-info"]/div[@id="smi-summary-items"]/span[@class="header_text"]'):
            text = sel.xpath('text()').extract()
            str = text[0].lower()
            house_type = re.search("house|apart|duplex|site|bunga", str)
            if (house_type):
                type = str
            beds = re.search("bed", str)
            if (beds):
                bed = str
        # Check if this is student accomodation
        for sel in response.xpath('//div[@id="description" and @class="description_block"]'):
            text = sel.xpath('text()').extract()
            str = text[0].lower()
            student_re = re.search("students* +only", str)
            if ( student_re):
                type = "student"
                print()
                print(colored('Found student accomodation "' + response.url + '"', 'yellow', attrs=['bold']))

        # Lat & Lon
        body = response.body.decode("latin1")
        lat_re = re.search('"latitude":.\d+.\d*', body)
        lat_digits = re.search("-*\d+.\d*", lat_re.group())
        lat_digits = lat_digits.group()
        lon_re = re.search('"longitude":.\d+.\d*', body)
        lon_digits = re.search("-*\d+.\d*", lon_re.group())
        lon_digits = lon_digits.group()
        # Floor
        for sel in response.xpath('//div[@class="description_block"]').extract():
            str = sel.lower()
            fl = re.search('overall floor area', str)
            if( fl==None ):
                continue
            fl = re.search("\d+.\d*.+sq.+met", str)
            if ( fl!=None ):
                digit = re.search("\d+", fl.group())
                floor = int(digit.group())
                break
            if ( floor!=None):
                break
        # Collect item
        item["url"] = response.url
        item["name"] = desc[0]
        item["price"] = price
        item["type"] = type
        item["beds"] = bed
        item["floor"] = floor
        item["lat"] = round(float(lat_digits), 8)
        item["lon"] = round(float(lon_digits), 8)
        print('.', end="", flush=True)
        yield item
