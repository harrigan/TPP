#!/usr/bin/python3
# -*- coding: utf-8 -*-
import scrapy
import re
from townspider.items import TownspiderItem


class TownSpider(scrapy.Spider):

    name = "town"
    allowed_domains = ["logainm.ie"]

    start_urls = [ 'https://www.logainm.ie/en/s?txt=in%3a100002&cat=BF&pag=-1&ord=en', ]
    #start_urls = [ 'https://www.logainm.ie/en/s?txt=in%3a100002&cat=BF&ord=en', ]

    def parse(self, response):
        l = response.xpath('//a[@class="name"]/@href').extract()
        for e in l:
            url = "https://www.logainm.ie" + e
            yield scrapy.Request(url, callback=self.parse_town)

    def parse_town_list(self, response):
        list = response.xpath('//div[@id="resultsList"]/a/@href').extract()
        for url in list:
            yield scrapy.Request( self.start_urls[0]+url, callback=self.parse_town )

    def parse_town(self, response):
        lat_str = response.xpath('//script[@type="text/javascript"]').re('placeLat=".\d+.\d+";')
        lon_str = response.xpath('//script[@type="text/javascript"]').re('placeLon=".\d+.\d+";')
        if( len(lat_str)!=1 or len(lon_str)!=1 ):
            print("ERROR: have no or more than single set of coordinates")
            return
        #TODO: review this regex in future to allow parsing any coords
        lat_d = re.search("\d+.\d+", lat_str[0])
        lon_d = re.search("-\d+.\d+", lon_str[0])
        item = TownspiderItem()
        item["lat"] = round(float(lat_d.group()), 8)
        item["lon"] = round(float(lon_d.group()), 8)
        name = response.xpath('//dd[@class="current"]//span[@class="nameL1"]/text()').extract()
        if ( len(name)!=1 ):
            print("ERROR: have no or more than single name %s" % name)
            return
        item["name"] = name[0]
        url = response.xpath('//dd/a/@href').extract()
        if ( len(url)!=1 ):
            print("ERROR: have no or more than single permanent link %s" % url)
            return
        item["uri"] = url[0]
        print('.', end="", flush=True)
        yield item



