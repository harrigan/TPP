# -*- coding: utf-8 -*-

# Define your item pipelines here
#
# Don't forget to add your pipeline to the ITEM_PIPELINES setting
# See: http://doc.scrapy.org/en/latest/topics/item-pipeline.html

from scrapy.exceptions import DropItem
from arango import ArangoClient

LATITUDE_BEGIN = 53.234407
LONGITUDE_BEGIN = -6.464235
LATITUDE_END = 53.463326
LONGITUDE_END = -6.041051


class PostprocessPipeline(object):
    def process_item(self, item, spider):
        #Check Dublin bounds
        if ( item["lat"] < LATITUDE_BEGIN or item["lat"] > LATITUDE_END ):
            raise DropItem("Latitude is outside of area in %s" % item)
        if ( item["lon"] < LONGITUDE_BEGIN or item["lon"] > LONGITUDE_END ):
            raise DropItem("Longitude is outside of area in %s" % item)
        return item


class DBPipeline(object):

    client = None
    col = None
    db = None

    def __init__(self, db_uri, db_name):
        self.client = ArangoClient(username='root',password='F33dback34',host=db_uri)
        self.db = self.client.db(db_name)
        self.col = self.db.collection('townlands')

    @classmethod
    def from_crawler(cls, crawler):
        return cls(
            db_uri = crawler.settings.get('DB_URI'),
            db_name = crawler.settings.get('DB_NAME')
        )

    def process_item(self, item, spider):
        price = {'name': item["name"], 'latitude': item["lat"], 'longitude': item["lon"], 'uri': item["uri"] }
        r = self.col.insert(price)
        print('.', end="", flush=True)
        return item


class CSVPipeline(object):

    file = None

    def __init__(self, town_file):
        self.file = open(town_file, 'wt')

    def __del__(self):
        self.file.flush()
        self.file.close()

    @classmethod
    def from_crawler(cls, crawler):
        return cls(
            town_file = crawler.settings.get('TOWN_FILE')
        )

    def process_item(self, item, spider):
        s = item["name"] + '|' + str(item["lat"]) + '|' + str(item["lon"]) + '|' + item["uri"] + '\n'
        self.file.write(s)
        print('.', end="", flush=True)
        return item
