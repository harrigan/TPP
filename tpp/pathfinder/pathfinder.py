import bisect

ROUTE_PROGRESS=0
ROUTE_FOUND=1
ROUTE_NOT_FOUND=-1

class Node:
    def __init__(self, n, p=None, d=.0, h=.0):
        self.n = n
        self.parent = p
        self.distance = d
        self.heuristic = h

    def w(self):
        return self.distance + self.heuristic

    def __lt__(self, rhs):
        return self.w() < rhs.w()

    def __lte__(self, rhs):
        return self.w() <= rhs.w()

    def __gt__(self, rhs):
        return self.w() > rhs.w()

    def __gte__(self, rhs):
        return self.w() >= rhs.w()

class Pathfinder:

    def __init__(self, get_successors_cb=None, measure_distance_cb=None):
        self.get_successors = get_successors_cb
        self.measure_distance = measure_distance_cb
        self.start_ = None
        self.end_ = None

    def init(self, start, end):
        self.start_ = Node(start, h=self.measure_distance(start, end))
        self.end_ = Node(end)
        self.open_nodes_ = [self.start_,]
        self.closed_nodes_ = []
        self.closed_nodes_ord_ = set()
        self.state_ = ROUTE_PROGRESS

    def step(self):
        if self.state_ == ROUTE_PROGRESS:
            if len(self.open_nodes_):
                best = self.open_nodes_.pop(0)
                self.closed_nodes_.append(best)
                for n, d in self.get_successors(best.n):
                    if n == self.end_.n:
                        self.state_ = ROUTE_FOUND
                        self.end_.parent = best
                        self.end_.distance = best.distance + d
                        break
                    elif n not in self.closed_nodes_ord_:
                        nx=Node(n, p=best, d=best.distance + d, h=self.measure_distance(n, self.end_.n))
                        bisect.insort_left(self.open_nodes_, nx)
                        self.closed_nodes_ord_.add(n)
            else:
                self.state_ = ROUTE_NOT_FOUND
        return self.state_

    def route(self):
        r = []
        d = 0
        if self.state_ == ROUTE_FOUND:
            nx = self.end_
            while nx:
                print(nx.n)
                r.append(nx.n)
                nx = nx.parent
        return (r[::-1], self.end_.distance)

    def find(self, start, end):
        self.init(start, end)
        while self.step() == ROUTE_PROGRESS:
            pass
        return self.route()
