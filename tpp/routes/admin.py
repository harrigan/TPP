from django.contrib import admin
from routes.models import *
from django.urls import reverse
from django.utils.html import format_html
from .views import show_trip, show_square_grid, show_stops_density_map

@admin.register(Stop)
class StopAdmin(admin.ModelAdmin):
    pass

class StopTimeInline(admin.TabularInline):
    model = StopTime
    extra = 0

class TripInline(admin.TabularInline):
    model = Trip
    extra = 0

class ShapePointInline(admin.TabularInline):
    model = ShapePoint
    extra = 0

class SquareGridCellInline(admin.TabularInline):
    model = SquareGridCell
    extra = 0
    readonly_fields = ['grid', 'min_lon', 'min_lat', 'max_lon', 'max_lat']
    can_delete = False

class StopsGraphEdgeInline(admin.TabularInline):
    model = StopsGraphEdge
    extra = 0
    can_delete = False

@admin.register(Trip)
class TripAdmin(admin.ModelAdmin):
    inlines = [
        StopTimeInline,
    ]

    def map_link(self):
        return format_html("<a href=\"{0}\">{0}</a>".format(reverse(show_trip, args=(self.id,))))
    map_link.short_description = "Map link"

    list_display = ('id', map_link, )

@admin.register(StopTime)
class StopTimeAdmin(admin.ModelAdmin):
    pass

@admin.register(Agency)
class AgencyAdmin(admin.ModelAdmin):
    pass

@admin.register(Route)
class RouteAdmin(admin.ModelAdmin):
    inlines = [
        TripInline,
    ]

@admin.register(Shape)
class ShapeAdmin(admin.ModelAdmin):
    inlines = [
        ShapePointInline,
    ]

@admin.register(SquareGrid)
class SquareGridAdmin(admin.ModelAdmin):
    inlines = [
        SquareGridCellInline,
    ]

    def map_link(self):
        return format_html("<a href=\"{0}\">{0}</a>".format(reverse(show_square_grid, args=(self.id,))))
    map_link.short_description = "Map link"

    def stops_density_link(self):
        return format_html("<a href=\"{0}\">{0}</a>".format(reverse(show_stops_density_map, args=(self.id,))))
    stops_density_link.short_description = "Stops density link"

    list_display = ('id', 'name', map_link, stops_density_link)

@admin.register(StopsGraph)
class StopsGraphAdmin(admin.ModelAdmin):
    inlines = [
        StopsGraphEdgeInline,
    ]

