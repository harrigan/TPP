from django.db import models
from django.db.models import F
from pyproj import Proj, transform

###############################################################################
#                                                                             #
#                       Transport route models.                               #
#                   Directly mapped from GTFS models.                         #
#                                                                             #
###############################################################################

class Stop(models.Model):
    id=models.CharField(verbose_name="Unique identifier", max_length=64, primary_key=True)
    name=models.CharField(verbose_name="Name", max_length=128)
    lon = models.DecimalField(verbose_name="Longitude", max_digits=10, decimal_places=7)
    lat = models.DecimalField(verbose_name="Latitude", max_digits=10, decimal_places=7)

    def __str__(self):
        return "(%s) %s" % (self.id, self.name)

class Agency(models.Model):
    id=models.CharField(verbose_name="Unique identifier", max_length=64, primary_key=True)
    name=models.CharField(verbose_name="Name", max_length=128)
    url=models.TextField(verbose_name="Name")
    timezone=models.CharField(verbose_name="Time zone", max_length=64)
    language=models.CharField(verbose_name="Language", max_length=16)
    phone=models.CharField(verbose_name="Phone", max_length=64)

class Shape(models.Model):
    id=models.CharField(verbose_name="Unique identifier", max_length=64, primary_key=True)

class ShapePoint(models.Model):
    shape=models.ForeignKey(Shape, on_delete=models.CASCADE, related_name="points")
    lon = models.DecimalField(verbose_name="Longitude", max_digits=10, decimal_places=7)
    lat = models.DecimalField(verbose_name="Latitude", max_digits=10, decimal_places=7)
    sequence=models.IntegerField(verbose_name="Sequence")
    dist_travelled=models.FloatField(verbose_name="Distance travelled in meters")

class Route(models.Model):
    id=models.CharField(verbose_name="Unique identifier", max_length=64, primary_key=True)
    agency=models.ForeignKey(Agency, on_delete=models.CASCADE)
    short_name=models.CharField(verbose_name="Short name", max_length=128)
    long_name=models.CharField(verbose_name="Long name", max_length=128)
    route_type=models.IntegerField(verbose_name="Type")

class Trip(models.Model):
    id=models.CharField(verbose_name="Unique identifier", max_length=64, primary_key=True)
    service_id=models.CharField(verbose_name="Service identifier", max_length=64)
    route=models.ForeignKey(Route, on_delete=models.CASCADE)
    headsign=models.CharField(verbose_name="Headsign", max_length=128)
    direction=models.IntegerField(verbose_name="Direction")
    shape=models.ForeignKey(Shape, on_delete=models.CASCADE, null=True)

    def __str__(self):
        return "(%s) %s - %s" % (self.id, self.headsign, self.direction)

class StopTime(models.Model):
    trip=models.ForeignKey(Trip, on_delete=models.CASCADE, related_name="stop_times")
    arrival_time=models.CharField(verbose_name="Arrival time", max_length=32)
    departure_time=models.CharField(verbose_name="Departure time", max_length=32)
    stop=models.ForeignKey(Stop, on_delete=models.CASCADE)
    sequence=models.IntegerField(verbose_name="Sequence")
    dist_travelled=models.FloatField(verbose_name="Distance travelled in meters")

class Transfer(models.Model):
    origin=models.ForeignKey(Stop, verbose_name="Origin stop", on_delete=models.CASCADE, related_name="+")
    target=models.ForeignKey(Stop, verbose_name="Target stop", on_delete=models.CASCADE, related_name="+")
    transfer_type=models.IntegerField(verbose_name="Transfer type")

###############################################################################
#                                                                             #
#                   Square grid used for calculations.                        #
#                                                                             #
###############################################################################

class SquareGrid(models.Model):
    name=models.CharField(verbose_name="Name", max_length=128)
    center_lon=models.DecimalField(verbose_name="Longitude", max_digits=10, decimal_places=7)
    center_lat=models.DecimalField(verbose_name="Latitude", max_digits=10, decimal_places=7)
    half_size=models.IntegerField(verbose_name="Half size")
    resolution=models.FloatField(verbose_name="Resolution")
    min_lon=models.DecimalField(verbose_name="Min longitude", max_digits=10, decimal_places=7)
    min_lat=models.DecimalField(verbose_name="Min latitude", max_digits=10, decimal_places=7)
    max_lon=models.DecimalField(verbose_name="Max longitude", max_digits=10, decimal_places=7)
    max_lat=models.DecimalField(verbose_name="Max latitude", max_digits=10, decimal_places=7)

    lon_lat_projection=Proj("+init=epsg:4326")
    flat_projection=None

    def __str__(self):
        return "%s @ lon=%s lat=%s" % (self.name, self.center_lon, self.center_lat)

    def save(self, *args, **kw):
        # delete existing cells
        self.cells.all().delete()

        super().save(*args, **kw)

        # project cells
        self.project_cells()

        super().save(*args, **kw)

    def project_cells(self):

        # should be odd
        if self.half_size % 2 == 0:
            self.half_size += 1

        self.min_lon = 999.9
        self.min_lat = 999.9
        self.max_lon = -999.9
        self.max_lat = -999.9

        x_o, y_o = self.project(self.center_lon, self.center_lat)

        for ix in range(self.half_size):
            for iy in range(self.half_size):
                x = x_o + (ix - 0.5 * self.half_size) * self.resolution
                y = y_o + (iy - 0.5 * self.half_size) * self.resolution

                min_lon, max_lat = self.unproject(x - 0.5 * self.resolution, y + 0.5 * self.resolution)
                max_lon, min_lat = self.unproject(x + 0.5 * self.resolution, y - 0.5 * self.resolution)

                self.min_lon = min(self.min_lon, min_lon)
                self.min_lat = min(self.min_lat, min_lat)
                self.max_lon = max(self.max_lon, max_lon)
                self.max_lat = max(self.max_lat, max_lat)

                self.cells.create(min_lon=min_lon, min_lat=min_lat, max_lon=max_lon, max_lat=max_lat, grid=self)

    def project(self, lon, lat):
        if not self.flat_projection:
            self.flat_projection = Proj("+proj=tmerc +lon_0=%s +lat_0=%s +ellps=WGS84 +units=m" % (self.center_lon, self.center_lat))
        return transform(self.lon_lat_projection, self.flat_projection, lon, lat)

    def unproject(self, x, y):
        if not self.flat_projection:
            self.flat_projection = Proj("+proj=tmerc +lon_0=%s +lat_0=%s +ellps=WGS84 +units=m" % (self.center_lon, self.center_lat))
        return transform(self.flat_projection, self.lon_lat_projection, x, y)

class SquareGridCell(models.Model):
    grid=models.ForeignKey(SquareGrid, on_delete=models.CASCADE, related_name="cells")
    min_lon=models.DecimalField(verbose_name="Min longitude", max_digits=10, decimal_places=7)
    min_lat=models.DecimalField(verbose_name="Min latitude", max_digits=10, decimal_places=7)
    max_lon=models.DecimalField(verbose_name="Max longitude", max_digits=10, decimal_places=7)
    max_lat=models.DecimalField(verbose_name="Max latitude", max_digits=10, decimal_places=7)

###############################################################################
#                                                                             #
#                               Stops graph.                                  #
# Stops can be either directly connected, i.e. they belong to the same trip   #
# w/ subsequent numbers, or not connected.                                    #
#                                                                             #
###############################################################################

class StopsGraph(models.Model):
    name=models.CharField(verbose_name="Name", max_length=64)
    grid=models.ForeignKey(SquareGrid, on_delete=models.CASCADE, related_name="+")

    def __str__(self):
        return "%s (%s)" % (self.name, self.id)

    def save(self, *args, **kw):
        # delete existing edges
        self.edges.all().delete()

        super().save(*args, **kw)

        self.fill_graph()

        super().save(*args, **kw)

    def fill_graph(self):
        stops_qs = Stop.objects.filter(lon__gte=self.grid.min_lon,
                                       lon__lte=self.grid.max_lon,
                                       lat__gte=self.grid.min_lat,
                                       lat__lte=self.grid.max_lat)

        print("Stops: %s" % stops_qs.count())
        for target_stop in stops_qs:

            for stop_time_related in StopTime.objects.filter(stop=target_stop):
                for origin in StopTime.objects.filter(trip=stop_time_related.trip, sequence__lt=stop_time_related.sequence).order_by('-sequence')[:1]:
                    if self.edges.filter(origin=origin.stop, target=target_stop, graph=self).count() == 0:
                        print("%s: %s -> %s (%s)" % (stop_time_related.trip, origin.stop.id, target_stop.id, stop_time_related.dist_travelled - origin.dist_travelled))
                        self.edges.create(origin=origin.stop,
                                          target=target_stop,
                                          graph=self,
                                          distance=stop_time_related.dist_travelled - origin.dist_travelled)

            for t in Transfer.objects.filter(origin=target_stop):
                if self.edges.filter(origin=t.origin, target=t.target).count() == 0:
                    print("transfer: %s -> %s" % (t.origin.id, t.target.id))
                    self.edges.create(origin=t.origin,
                                      target=t.target,
                                      graph=self,
                                      distance=0)

class StopsGraphEdge(models.Model):
    origin=models.ForeignKey(Stop, verbose_name="Origin stop", on_delete=models.CASCADE, related_name="+")
    target=models.ForeignKey(Stop, verbose_name="Target stop", on_delete=models.CASCADE, related_name="+")
    graph=models.ForeignKey(StopsGraph, on_delete=models.CASCADE, related_name="edges")
    distance=models.FloatField(verbose_name="Distance")

    def __str__(self):
        return "(%s) %s -> %s" % (self.id, self.origin, self.target)
