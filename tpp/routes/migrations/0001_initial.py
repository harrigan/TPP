# Generated by Django 2.0.1 on 2018-01-19 12:00

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Stop',
            fields=[
                ('id', models.IntegerField(primary_key=True, serialize=False, verbose_name='Unique identifier')),
                ('name', models.CharField(max_length=128, verbose_name='Name')),
                ('lon', models.DecimalField(decimal_places=7, max_digits=10, verbose_name='Longitude')),
                ('lat', models.DecimalField(decimal_places=7, max_digits=10, verbose_name='Latitude')),
            ],
        ),
    ]
