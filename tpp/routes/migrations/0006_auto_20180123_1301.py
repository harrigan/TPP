# Generated by Django 2.0.1 on 2018-01-23 13:01

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('routes', '0005_auto_20180123_1300'),
    ]

    operations = [
        migrations.RenameField(
            model_name='trip',
            old_name='route_id',
            new_name='route',
        ),
    ]
