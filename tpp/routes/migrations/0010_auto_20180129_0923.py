# Generated by Django 2.0.1 on 2018-01-29 09:23

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('routes', '0009_stopsgraph_stopsgraphedge'),
    ]

    operations = [
        migrations.AddField(
            model_name='squaregrid',
            name='max_lat',
            field=models.DecimalField(decimal_places=7, default=0, max_digits=10, verbose_name='Max latitude'),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='squaregrid',
            name='max_lon',
            field=models.DecimalField(decimal_places=7, default=0, max_digits=10, verbose_name='Max longitude'),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='squaregrid',
            name='min_lat',
            field=models.DecimalField(decimal_places=7, default=0, max_digits=10, verbose_name='Min latitude'),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='squaregrid',
            name='min_lon',
            field=models.DecimalField(decimal_places=7, default=0, max_digits=10, verbose_name='Min longitude'),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name='stopsgraph',
            name='grid',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='+', to='routes.SquareGrid'),
        ),
        migrations.AlterField(
            model_name='stopsgraphedge',
            name='graph',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='edges', to='routes.StopsGraph'),
        ),
    ]
