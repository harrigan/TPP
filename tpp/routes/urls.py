from django.urls import path
from routes import views

urlpatterns = [
    path('stops/', views.showstops),
    path('stops/closest/<int:graph_id>/<lon>/<lat>/', views.closest_stop),
    path('stops/density/<int:grid_id>/', views.show_stops_density_map),
    path('stops/graph/<int:graph_id>/', views.show_stops_graph),
    path('stops/graph/<int:graph_id>/pathfinder/', views.show_pathfinder),
    path('stops/graph/<int:graph_id>/pathfinder/<from_stop_id>/<to_stop_id>/', views.find_path),
    path('trip/<id>/', views.show_trip),
    path('grid/<int:id>/', views.show_square_grid),
]
