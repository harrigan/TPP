import math
import json
from pyproj import Proj, transform
from django.shortcuts import render
from django.http import HttpResponse
from routes.models import *
from pathfinder.pathfinder import Pathfinder


def showstops(request):
    return render(request, "routes/allstops.html", {'stops': Stop.objects.all() })

def show_trip(request, id):
    return render(request, "routes/showtrip.html", {'trip': Trip.objects.get(pk=id) })

def show_square_grid(request, id):
    return render(request, "routes/showsquaregrid.html", {'grid': SquareGrid.objects.get(pk=id) })

def show_stops_density_map(request, grid_id):

    grid = SquareGrid.objects.get(id=grid_id)

    data = []

    max_stops = -1
    for c in grid.cells.all():
        n = Stop.objects.filter(lon__gte=c.min_lon, lon__lte=c.max_lon, lat__gte=c.min_lat, lat__lte=c.max_lat).count()
        max_stops = max(max_stops, n)
        data.append([c, n, n])

    for r in data:
        if r[2] > 0:
            r[2] = 0.25 + 0.75 * r[2] / max_stops

    return render(request, "routes/showstopsdensitymap.html", {'grid': data})

def show_stops_graph(request, graph_id):
    graph = StopsGraph.objects.get(pk=graph_id)
    stops = Stop.objects.filter(lon__gte=graph.grid.min_lon,
                                lon__lte=graph.grid.max_lon,
                                lat__gte=graph.grid.min_lat,
                                lat__lte=graph.grid.max_lat)
    return render(request, "routes/showstopsgraph.html", {'graph': graph, 'stops': stops})

def show_pathfinder(request, graph_id):
    graph = StopsGraph.objects.get(pk=graph_id)
    stops = Stop.objects.filter(lon__gte=graph.grid.min_lon,
                                lon__lte=graph.grid.max_lon,
                                lat__gte=graph.grid.min_lat,
                                lat__lte=graph.grid.max_lat)
    return render(request, "routes/pathfinder.html", {'graph': graph, 'stops': stops})

def find_path(request, graph_id, from_stop_id, to_stop_id):

    graph = StopsGraph.objects.get(id=graph_id)

    def get_successors(s):
        for e in StopsGraphEdge.objects.filter(graph=graph, origin=s):
            yield e.target, e.distance

    grid = graph.grid

    start = Stop.objects.get(id=from_stop_id)
    end = Stop.objects.get(id=to_stop_id)

    x_e, y_e = grid.project(end.lon, end.lat)

    def measure_distance(b, e):
        assert(end == e)
        x, y = grid.project(b.lon, b.lat)
        dx, dy = x - x_e, y - y_e
        return math.sqrt(dx * dx + dy * dy)

    p = Pathfinder(get_successors, measure_distance)

    path, distance = p.find(start=start, end=end)

    r = dict(distance=distance,
             path=[dict(lon=float(s.lon), lat=float(s.lat)) for s in path])

    return HttpResponse(json.dumps(r), content_type="application/json")

def closest_stop(request, graph_id, lon, lat):

    grid = StopsGraph.objects.get(id=graph_id).grid
    x, y = grid.project(lon, lat)

    d = 500

    min_lon, max_lat = grid.unproject(x - 0.5 * d, y + 0.5 * d)
    max_lon, min_lat = grid.unproject(x + 0.5 * d, y - 0.5 * d)

    stop = None
    stop_dist = 0

    for s in Stop.objects.filter(lon__gte=min_lon, lon__lte=max_lon, lat__gte=min_lat, lat__lte=max_lat):
        xs, ys = grid.project(s.lon, s.lat)
        dx, dy = xs - x, ys - y
        dist = math.sqrt(dx * dx + dy * dy)
        if stop is None or dist < stop_dist:
            stop = s
            stop_dist = dist

    if stop is not None:
        return HttpResponse(json.dumps(dict(id=stop.id, title=stop.name, lon=float(stop.lon), lat=float(stop.lat))), content_type="application/json")

    return HttpResponse(json.dumps(None), content_type="application/json")

