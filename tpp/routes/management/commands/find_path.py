import math
from django.core.management.base import BaseCommand, CommandError
from django.db.models import Q
from routes.models import *
from pathfinder.pathfinder import Pathfinder

class Command(BaseCommand):
    help = 'Finds path between two stops'

    def add_arguments(self, parser):
        parser.add_argument('graph_id', type=int)
        parser.add_argument('start_id', type=str)
        parser.add_argument('stop_id', type=str)

    def handle(self, *args, **kw):

        def get_successors(s):
            for e in StopsGraphEdge.objects.filter(origin=s):
                yield e.target, e.distance

        grid = StopsGraph.objects.get(id=int(kw["graph_id"])).grid

        start = Stop.objects.get(id=kw["start_id"])
        end = Stop.objects.get(id=kw["stop_id"])

        x_e, y_e = grid.project(end.lon, end.lat)

        def measure_distance(b, e):
            assert(end == e)
            x, y = grid.project(b.lon, b.lat)
            dx, dy = x - x_e, y - y_e
            return math.sqrt(dx * dx + dy * dy)

        p = Pathfinder(get_successors, measure_distance)

        r = p.find(start=Stop.objects.get(id=kw["start_id"]),
                   end=Stop.objects.get(id=kw["stop_id"]))

        print(r)
