def gtfs_isplit(s):
    m = 'i'
    token = ""
    for c in s:
        if c == '"':
            if m == 'q':
                m = 'i'
            else:
                m = 'q'
        elif c == ',' and m == 'i':
            yield token
            token = ""
        else:
            token += c
    if len(token):
        yield token

gtfs_split = lambda s: list(gtfs_isplit(s))

if __name__ == '__main__':
    print(list(gtfs_split('shape_id,shape_pt_lat,shape_pt_lon,shape_pt_sequence,shape_dist_traveled')))
    print(list(gtfs_split('"60-102-b12-1.77.O","53.3926886459835","-6.1188548446127","2","209.153054005912"')))
    print(list(gtfs_split('"60-1-b12-1",978,1,,3')))
    print(list(gtfs_split('"8220DB000008","Rotunda, Parnell Square West","53.3532722714822","-6.265183676574"')))
