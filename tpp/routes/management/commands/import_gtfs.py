from decimal import Decimal
import urllib.request
import zipfile
from django.core.management.base import BaseCommand, CommandError
from django.db.transaction import atomic
from routes.models import *
from .util import gtfs_isplit, gtfs_split

class Command(BaseCommand):
    help = 'Imports routes data from GTFS source'

    def add_arguments(self, parser):
        parser.add_argument('url', type=str)

    @atomic
    def handle(self, *args, **kw):
        with urllib.request.urlopen(kw['url']) as src:
            print("Collecting %s" % src.url)
            print("--------------------------")
            print(src.info())
            print("--------------------------")
            source = None
            if src.info()['Content-Type'] == 'application/zip':
                with zipfile.ZipFile(src) as z:
                    with z.open("agency.txt") as f:
                        self.process_agencies_file(f)
                    with z.open("shapes.txt") as f:
                        self.process_shapes_file(f)
                    with z.open("routes.txt") as f:
                        self.process_routes_file(f)
                    with z.open("stops.txt") as f:
                        self.process_stops_file(f)
                    with z.open("trips.txt") as f:
                        self.process_trips_file(f)
                    with z.open("stop_times.txt") as f:
                        self.process_stop_times_file(f)
                    with z.open("transfers.txt") as f:
                        self.process_transfers_file(f)

    def process_file(self, f):
        head = gtfs_split(f.readline().decode("utf-8-sig").strip("\r\n"))
        for l in f.readlines():
            info = {}
            for i, fvalue in enumerate(gtfs_isplit(l.decode("utf-8-sig").strip("\r\n"))):
                info.update({head[i % len(head)]: fvalue.strip('"')})
            yield info

    def process_trips_file(self, f):
        for info in self.process_file(f):
            t = Trip(id=info["trip_id"],
                     service_id=info["service_id"],
                     route=Route.objects.get(pk=info["route_id"]),
                     headsign=info["trip_headsign"],
                     shape=Shape.objects.get(pk=info["shape_id"]),
                     direction=int(info["direction_id"]))
            t.save()

    def process_stop_times_file(self, f):
        for info in self.process_file(f):
            t = StopTime(trip=Trip.objects.get(pk=info["trip_id"]),
                         arrival_time=info["arrival_time"],
                         departure_time=info["departure_time"],
                         stop=Stop.objects.get(pk=info["stop_id"]),
                         sequence=int(info["stop_sequence"]),
                         dist_travelled=float(info["shape_dist_traveled"]))
            t.save()

    def process_stops_file(self, f):
        for info in self.process_file(f):
            s = Stop(id=info["stop_id"],
                     name=info["stop_name"],
                     lon=Decimal(info["stop_lon"]),
                     lat=Decimal(info["stop_lat"]))
            s.save()

    def process_agencies_file(self, f):
        for info in self.process_file(f):
            a = Agency(id=info["agency_id"],
                       name=info["agency_name"],
                       timezone=info["agency_timezone"],
                       language=info["agency_lang"],
                       phone=info["agency_phone"])
            a.save()

    def process_routes_file(self, f):
        for info in self.process_file(f):
            r = Route(id=info["route_id"],
                      agency=Agency.objects.get(pk=info["agency_id"]),
                      short_name=info["route_short_name"],
                      long_name=info["route_long_name"],
                      route_type=int(info["route_type"]))
            r.save()

    def process_shapes_file(self, f):
        for info in self.process_file(f):
            shp = None
            try:
                shp = Shape.objects.get(pk=info["shape_id"])
            except:
                shp = Shape(id=info["shape_id"])
                shp.save()
            shp.points.create(shape=shp,
                              lon=info["shape_pt_lon"],
                              lat=info["shape_pt_lat"],
                              sequence=int(info["shape_pt_sequence"]),
                              dist_travelled=float(info["shape_dist_traveled"]))
            shp.save()

    def process_transfers_file(self, f):
        for info in self.process_file(f):
            Transfer.objects.create(origin=Stop.objects.get(pk=info["from_stop_id"]),
                                    target=Stop.objects.get(pk=info["to_stop_id"]),
                                    transfer_type=int(info["transfer_type"]))
