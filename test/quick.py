#!/usr/bin/python3

import re

i = 1001
c = 0
while (i<10000):
    s = str(i)
    r = re.search('([1-9]{1}0{1}[1-9]{2})|([1-9]{3}0{1})|([1-9]{2}0{1}[1-9]{1})',s)
    if (r):
        c+=1
    i+=1
print (c)