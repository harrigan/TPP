#!/usr/bin/python3

import nltk
from nltk.corpus import conll2000
from nltk.chunk import util

text = "Loaded gun and drugs worth €10k seized in Dublin raid, two arrested - Independent.ie " \
       "Gardaí have seized a loaded firearm and drugs during a search in Tallaght this afternoon. " \
       "A man and a woman were also arrested at the scene." \
       "In a statement gardaí said the search was part of an ongoing operation targeting the illegal " \
       "sale and supply of controlled drugs in the Tallaght area.Uniformed Gardaí attached to Tallaght " \
       "station carried out the planned search of a unit located at an Industrial Park, Cookstown shortly after 1pm. " \
       "A loaded firearm, cannabis herb with an estimated value of €10,000, pending analysis and a small quantity of " \
       "suspected cocaine along with drug paraphernalia associated with grow houses were seized during the search. " \
       "A man, aged in his early 40s and a woman in her mid 30s were arrested at scene. Both are currently detained at " \
       "Tallaght Garda Station under Section 4 of the Criminal Justice Act, 1984."

vp_pattern = """CP: {<CD|RB><NN>?}
                VP: {<VB|VB.><RP|IN><TO>}
                VP: {<VB><TO><VB>}
                VP: {<JJ*>*<NN|NNP>?<TO|IN>?<VB|VB.><DT>?}
                NP: {<NNP><JJ>}
                NP: {<NN><TO>?<NNP>}
                NP: {<DT>?<JJ*>*<NN|NN.>*}"""


class UnigramChunker(nltk.ChunkParserI):
    def __init__(self, train_sents): # [_code-unigram-chunker-constructor]
        train_data = [[(t,c) for w,t,c in nltk.chunk.tree2conlltags(sent)]
                      for sent in train_sents]
        self.tagger = nltk.UnigramTagger(train_data) # [_code-unigram-chunker-buildit]

    def parse(self, sentence): # [_code-unigram-chunker-parse]
        pos_tags = [pos for (word,pos) in sentence]
        tagged_pos_tags = self.tagger.tag(pos_tags)
        chunktags = [chunktag for (pos, chunktag) in tagged_pos_tags]
        conlltags = [(word, pos, chunktag) for ((word,pos),chunktag)
                     in zip(sentence, chunktags)]
        return nltk.chunk.conlltags2tree(conlltags)


words = nltk.word_tokenize(text)
tags = nltk.pos_tag(words)
chunker = nltk.RegexpParser(vp_pattern)
result = chunker.parse(tags)
result1 = nltk.chunk.util.tree2conlltags(result)
chunks = nltk.ne_chunk(result1, binary=True)
for c in chunks:
    if (hasattr(c, '_label') and c._label=='NE'):
        print(c)
quit()

nltk.chunk.conllstr2tree(result1, chunk_types=['NP']).draw()

train_sents = conll2000.chunked_sents('train.txt', chunk_types=['NP'])
test_sents = conll2000.chunked_sents('test.txt', chunk_types=['NP'])
unigram_chunker = UnigramChunker(train_sents)
print(unigram_chunker.evaluate(test_sents))

#nltk.chunk.conllstr2tree(text, chunk_types=['NP']).draw()
#words = nltk.word_tokenize(text)
#tags = nltk.pos_tag(words)
#chunker = nltk.RegexpParser(vp_pattern)
#result = chunker.parse(tags)
#result = None
