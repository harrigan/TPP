#!/usr/bin/python3

from arango import ArangoClient
import argparse
import math
import datetime
import json
from termcolor import colored
import subprocess


def house_size( size ):
   if (size < 80):
        return "small"
   elif (size < 130):
        return "medium"
   elif (size < 180):
        return "large"
   else:
        return "huge"


def validate_sale(report):
    # Read the price collection
    client = ArangoClient(username='admin',password='F33dback34')
    db = client.database(name="dublin")
    prices_col = db.collection(name="property_sale")
    query = "FOR d IN property_sale FILTER d.floor > 400 RETURN d"
    records = db.aql.execute(query, ttl=7200)
    stats = records.statistics()
    print("Retrieved %d properties" % stats['scanned_index'] )

    # Process records
    for rec in records:
        sqm_price = int(rec['price']/rec['floor'])
        if ( sqm_price < 1500 ):
            print( colored('WARNING: Square meter price ' + str(sqm_price) + ' is less than 1500 euro','white','on_red',attrs=['bold']))
            print( json.dumps(rec, sort_keys=True, indent=4) )
            # If this is foot instead of meter typo then fix it automatically
            if (rec['beds']<5 and rec['floor']>500):
                print("Converting square foot to square meters...")
                rec['floor'] = int(rec['floor'] * 0.092903044)
                rec['size'] = house_size(rec['floor'])
                r = prices_col.update(rec)
                print()
                continue
            print( colored('Do you want to swap foot/metre[f] or delete[d] or edit[e] or skip[↵] the record?','white','on_blue',attrs=['bold'])+' ', end='' )
            i = input()
            if (i.lower()=='f' or i.lower()=='feet'):
                print( "Converting square foot to square meters..." )
                rec['floor'] = int(rec['floor']*0.092903044)
                rec['size'] = house_size( rec['floor'] )
                r = prices_col.update(rec)
                print()
            elif (i.lower()=='d' or i.lower()=='delete'):
                print( "Deleting..." )
                r = prices_col.delete(rec['_key'])
                print()
            elif (i.lower()=='e' or i.lower()=='edit'):
                print( "Manually editing..." )
                print()
                uri = 'http://127.0.0.1:8529/_db/dublin/_admin/aardvark/index.html#collection/property_sale/' + rec['_key']
                subprocess.run(['firefox',uri])
            else:
                print( "Skipping..." )
                print()
                continue

def validate_rent(report):
    # Read the price collection
    client = ArangoClient(username='admin',password='F33dback34')
    db = client.database(name="dublin")
    prices_col = db.collection(name="property_rent")
    query = "FOR d IN property_rent FILTER d.beds > 10 RETURN d"
    records = db.aql.execute(query, ttl=7200)
    print("Analyzing dwellings with unrealistic rooms...")

    # Process records
    for rec in records:
        print(colored('WARNING: Too many rooms in rented house: ' + str(rec['beds']), 'white', 'on_red', attrs=['bold']))
        print(json.dumps(rec, sort_keys=True, indent=4))
        print(colored('Do you want to label as a room [r] or delete [d] or edit [e] or skip [↵] the record?', 'white','on_blue', attrs=['bold']) + ' ', end='')
        i = input()
        if (i.lower() == 'r' or i.lower() == 'room'):
            print("Labelling dwelling as a room for rent...")
            rec['beds'] = 1
            rec['size'] = 'small'
            r = prices_col.update(rec)
            print()
        elif (i.lower() == 'd' or i.lower() == 'delete'):
            print("Deleting...")
            r = prices_col.delete(rec['_key'])
            print()
        elif (i.lower() == 'e' or i.lower() == 'edit'):
            print("Manually editing...")
            print()
            uri = 'http://127.0.0.1:8529/_db/dublin/_admin/aardvark/index.html#collection/property_rent/' + rec['_key']
            subprocess.run(['firefox', uri])
        else:
            print("Skipping...")
            print()
            continue

    query = "FOR d IN property_rent FILTER d.price < 1000 and d.beds > 1 RETURN d"
    records = db.aql.execute(query, ttl=7200)
    print("Analyzing dwellings with unrealistic prices...")
    # Process records
    for rec in records:
        print(colored('WARNING: Too low price: ' + str(rec['price']), 'white', 'on_red', attrs=['bold']))
        print(json.dumps(rec, sort_keys=True, indent=4))
        bedprice = rec['price']/rec['beds']
        # If this is just single room for rent then fix it automatically
        if (bedprice < 350):
            print("Most likely this is a room for rent...")
            rec['beds'] = 1
            rec['size'] = 'small'
            r = prices_col.update(rec)
            print()
            continue
        print(colored('Do you want to label as a room [r] or delete [d] or edit [e] or skip [↵] the record?', 'white','on_blue', attrs=['bold']) + ' ', end='')
        i = input()
        if (i.lower() == 'r' or i.lower() == 'room'):
            print("Labelling dwelling as a room for rent...")
            rec['beds'] = 1
            rec['size'] = 'small'
            r = prices_col.update(rec)
            print()
        elif (i.lower() == 'd' or i.lower() == 'delete'):
            print("Deleting...")
            r = prices_col.delete(rec['_key'])
            print()
        elif (i.lower() == 'e' or i.lower() == 'edit'):
            print("Manually editing...")
            print()
            uri = 'http://127.0.0.1:8529/_db/dublin/_admin/aardvark/index.html#collection/property_rent/' + rec['_key']
            subprocess.run(['firefox', uri])
        else:
            print("Skipping...")
            print()
            continue



parser = argparse.ArgumentParser()
parser.add_argument("--sale", action='store_true', help='Dwellings for sale')
parser.add_argument("--rent", action='store_true', help='Dwellings for rent')
parser.add_argument("--file", nargs='?', help='Report file name', default='db_artifacts_report.log')
args = parser.parse_args()
if (args.sale):
    validate_sale(report=args.file)
elif (args.rent):
    validate_rent(report=args.file)
else:
    print("One of --sale or --rent validator modes must be used")
