#!/usr/bin/python3

from arango import ArangoClient

LATITUDE_BEGIN = 53.234407
LONGITUDE_BEGIN = -6.464235
LATITUDE_END = 53.463326
LONGITUDE_END = -6.041051
STEP = 0.004  # Approx 100 meters step

#Creates empty DB
def create_db():
    # Initialize the client for ArangoDB
    client = ArangoClient(username='admin', password='F33dback34')
    db = client.database(name='dublin', username='admin', password='F33dback34')
    #db = client.create_database('dublin')
    # Create a new user with access to "my_database"
    #client.create_user('admin', 'F33dback34')
    #client.grant_user_access('admin', 'dublin')
    col = db.create_collection('grid_200m')
    #index = col.add_geo_index(fields=[ "latitude", "longitude" ])
    #ref_col = db.create_collection('price')
    c = 0
    lon = LONGITUDE_BEGIN
    while lon < LONGITUDE_END:
        lat = LATITUDE_BEGIN
        while lat < LATITUDE_END:
            lat += STEP
            uref = 'dublin_' + str(c)
            doc = {'latitude':round(float(lat), 8), 'longitude': round(float(lon), 8), 'price_ref': uref }
            res = col.insert(doc)
            c += 1
        lon += STEP
        print('.', end="", flush=True)


create_db()