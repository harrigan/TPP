#!/usr/bin/python3

from arango import ArangoClient
import argparse

class PriceItem:
    avg_price = 0
    num = 0


class Size:

    small = None
    small_str = "small"
    medium = None
    medium_str = "medium"
    large = None
    large_str = "large"
    huge = None
    huge_str = "huge"

    def __eq__(self, other):
        if ( self.small and other==self.small_str ):
            return True
        elif ( self.medium and other==self.medium_str ):
            return True
        elif ( self.large and other==self.large_str ):
            return True
        elif ( self.huge and other==self.huge_str ):
            return True
        else:
            return False


def average_calc( prices, size ):
    # Sum all prices
    avg_prices = dict()
    for price in prices:
        if ( price["size"]==size ):
            if ( price["reference"] not in avg_prices ):
                avg_prices[price["reference"]] = PriceItem()
            avg_prices[price["reference"]].avg_price += price["price"]
            avg_prices[price["reference"]].num += 1

    # Average all prices
    for ref in avg_prices:
        avg_prices[ref].avg_price = int(avg_prices[ref].avg_price/avg_prices[ref].num)
    return avg_prices


def read_avg_prices( size, price_col_name, from_date=None, to_date=None ):
    # Read the price collection
    client = ArangoClient(username='admin',password='F33dback34')
    db = client.database(name="dublin")
    price_col = db.collection(name=price_col_name)
    price_col.load()
    if( from_date and to_date ):
        query = "FOR d IN " + price_col_name + " FILTER d.date > '" + from_date + "' AND d.date < '" + to_date + "' RETURN d"
        prices = db.aql.execute(query)
    else:
        prices = price_col.all()
    grid_col = db.collection(name="grid_200m")
    grid_col.load()
    grid = grid_col.all()

    # Sum all prices
    avg_prices = average_calc( prices=prices, size=size )

    # Iterate over grid
    for g in grid:
        if( g["price_ref"] in avg_prices ):
            print( g["price_ref"] + '|' + str(g["latitude"]) + '|' + str(g["longitude"]) + '|' + str(avg_prices[g["price_ref"]].avg_price) )



def compare_avg_prices( size, price_col_name, a1, a2, b1, b2 ):
    # Read the price collection
    client = ArangoClient(username='admin',password='F33dback34')
    db = client.database(name="dublin")
    price_col = db.collection(name=price_col_name)
    price_col.load()
    grid_col = db.collection(name="grid_200m")
    grid_col.load()
    grid = grid_col.all()

    # Sum all prices
    query = "FOR d IN " + price_col_name + " FILTER d.date > '" + a1 + "' AND d.date < '" + a2 + "' RETURN d"
    prices_from = db.aql.execute(query)
    avg_prices_from = average_calc( prices=prices_from, size=size )
    query = "FOR d IN " + price_col_name + " FILTER d.date > '" + b1 + "' AND d.date < '" + b2 + "' RETURN d"
    prices_to = db.aql.execute(query)
    avg_prices_to = average_calc( prices=prices_to, size=size )

    # Iterate over grid
    for g in grid:
        if( g["price_ref"] in avg_prices_from and g["price_ref"] in avg_prices_to ):
            diff = avg_prices_to[g["price_ref"]].avg_price - avg_prices_from[g["price_ref"]].avg_price
            print( g["price_ref"] + '|' + str(g["latitude"]) + '|' + str(g["longitude"]) + '|' + str(diff) )


parser = argparse.ArgumentParser()
parser.add_argument("--small", action='store_true', help="Output small size (up to 80 sq. m floor size) dwellings")
parser.add_argument("--medium", action='store_true', help="Output medium size (up to 130 sq. m floor size) size dwellings")
parser.add_argument("--large", action='store_true', help="Output large size (up to 180 sq. m floor size) dwellings")
parser.add_argument("--huge", action='store_true', help="Output huge size (more than 180 sq. m floor size)) dwellings")
parser.add_argument("--compare", nargs='+', help='Compare different dates a1 > x < a2 with b1 > x < b2 (dates must be in YYYY-MM-DD format)')
parser.add_argument("--dates", nargs='+', help='Specify date range a1 > x < a2 (dates must be in YYYY-MM-DD format)')
parser.add_argument("--sale", action='store_true', help='Dwellings for sale')
parser.add_argument("--rent", action='store_true', help='Dwellings for rent')
args = parser.parse_args()
s = Size()
s.small = args.small
s.medium = args.medium
s.large = args.large
s.huge = args.huge
price_col_name = None
if( args.sale ):
    price_col_name = 'price_200m'
elif( args.rent ):
    price_col_name = 'rent_200m'
else:
    print("ERROR: Sale or rent map type is not specified, please use --sale or --rent")
    quit()
if ( args.compare ):
    if ( len(args.compare)!=4 ):
        print ("ERROR: Compare mode requires exactly four dates in YYYY-MM-DD format")
        quit()
    compare_avg_prices( size=s, price_col_name=price_col_name, a1=args.compare[0], a2=args.compare[1], b1=args.compare[2], b2=args.compare[3])
else:
    if ( args.dates ):
        if( len(args.dates)!=2 ):
            print("ERROR: Date range requires exactly two dates in YYYY-MM-DD format")
            quit()
        read_avg_prices( size=s, price_col_name=price_col_name, from_date=args.dates[0], to_date=args.dates[1] )
    else:
        read_avg_prices( size=s, price_col_name=price_col_name )

