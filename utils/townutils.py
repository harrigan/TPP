#!/usr/bin/python3

from arango import ArangoClient
import argparse
import math
import datetime
import sys
from array import array

class PriceRecord:
    small_val = 0
    small_num = 0
    small_avg = 0
    small_floor = 0
    small_floor_num = 0
    medium_val = 0
    medium_num = 0
    medium_avg = 0
    medium_floor = 0
    medium_floor_num = 0
    large_val = 0
    large_num = 0
    large_avg = 0
    large_floor = 0
    large_floor_num = 0
    huge_val = 0
    huge_num = 0
    huge_avg = 0
    huge_floor = 0
    huge_floor_num = 0

    def process(self, rec):
        if ( rec['size']=="small" ):
            self.small_val += rec['price']
            self.small_num += 1
            # To remove doggy floor sizes we remove everything less than 30 sq. meters as there could be a typo
            if ( rec['floor']!=None and rec['floor']>30 ):
                self.small_floor += rec['price']/rec['floor']
                self.small_floor_num += 1
        elif ( rec['size']=="medium" ):
            self.medium_val += rec['price']
            self.medium_num += 1
            if ( rec['floor']!=None and rec['floor']>30 ):
                self.medium_floor += rec['price']/rec['floor']
                self.medium_floor_num += 1
        elif (rec['size'] == "large"):
            self.large_val += rec['price']
            self.large_num += 1
            if ( rec['floor']!=None and rec['floor']>30 ):
                self.large_floor += rec['price']/rec['floor']
                self.large_floor_num += 1
        elif ( rec['size']=="huge" ):
            self.huge_val += rec['price']
            self.huge_num += 1
            if ( rec['floor']!=None and rec['floor']>30 ):
                self.huge_floor += rec['price']/rec['floor']
                self.huge_floor_num += 1
        else:
            print("No size is defined: %s" % rec)

    def average(self):
        if( self.small_num > 0 ):
            self.small_avg = math.floor(self.small_val / self.small_num)
        if( self.small_floor > 0 ):
            self.small_floor = math.floor(self.small_floor / self.small_floor_num)
        if (self.medium_num > 0):
            self.medium_avg = math.floor(self.medium_val / self.medium_num)
        if( self.medium_floor > 0 ):
            self.medium_floor = math.floor(self.medium_floor / self.medium_floor_num)
        if (self.large_num > 0):
            self.large_avg = math.floor(self.large_val / self.large_num)
        if( self.large_floor > 0 ):
            self.large_floor = math.floor(self.large_floor / self.large_floor_num)
        if (self.huge_num > 0):
            self.huge_avg = math.floor(self.huge_val / self.huge_num)
        if( self.huge_floor > 0 ):
            self.huge_floor = math.floor(self.huge_floor / self.huge_floor_num)


class RentRecord:
    val = None
    num = None
    avg = None

    def __init__(self):
        self.val = dict()
        self.num = dict()
        self.avg = dict()

    def process(self, rec):
        if ( rec['beds']==None or rec['beds']==0 ):
            print( 'No beds specified in "' + rec['name'] + '"' )
            return
        if ( rec['beds'] not in self.val ):
            self.val[rec['beds']] = 0
            self.num[rec['beds']] = 0
            self.avg[rec['beds']] = 0
        self.val[rec['beds']] += rec['price']
        self.num[rec['beds']] += 1

    def average(self):
        for i in self.val:
            if (self.num[i] > 0):
                self.avg[i] = math.floor(self.val[i] / self.num[i])


def extract_town(name, towns):
    for t in towns:
        if( name.find(t)!=-1 ):
            return t
    return None

def progress(started, i):
    finished = datetime.datetime.now()
    td = finished - started
    rep = "Processed " + str(i) + " items in " + str(td.total_seconds()) + " seconds"
    print(rep)


def aggregate_sale(report, a, b, label, town = None):
    # Read the price collection
    client = ArangoClient(username='admin',password='F33dback34')
    db = client.database(name="dublin")
    prices_col = db.collection(name="property_sale")
    if town:
        query = "FOR d IN property_sale FILTER d.date >= '" + a + "' AND d.date <= '" + b + \
                "' AND LIKE(LOWER(d.town),LOWER('" + town + "')) RETURN d"
    else:
        query = "FOR d IN property_sale FILTER d.date >= '" + a + "' AND d.date <= '" + b + "' RETURN d"
    prices = db.aql.execute(query, ttl=7200)
    towns = db.collection(name="towns")
    towns.load()
    tlist = list()
    for t in towns:
        tlist.append(t['name'])
    a = prices.statistics()
    print("Retrieved %d properties" % a['scanned_index'] )

    # Prepare placeholder for the aggregation
    aggregation = dict()
    for t in tlist:
        aggregation[t] = PriceRecord()

    # Process records
    i = 0
    started = datetime.datetime.now()
    for rec in prices:
        t = None
        if ("town" not in rec):
            t = extract_town( name = rec["name"], towns = tlist)
        else:
            t = rec["town"]
        if ( t == None ):
            print("No town is defined: %s" % rec)
            continue
        if ( t not in aggregation ):
            aggregation[t] = PriceRecord()
        aggregation[t].process( rec = rec )
        i += 1
        rem = i/1000
        c = rem - math.floor(rem)
        if ( c==0 ):
            progress(started=started, i=i)
    progress(started=started, i=i)

    # Print the report
    file = open(report, 'a')
    #file.write("town|date|small_pp|medium_pp|large_pp|huge_pp|small_sqmp|medium_sqmp|large_sqmp|huge_sqmp\n")
    for a in aggregation:
        if ( town and a != town ):
            continue
        aggregation[a].average()
        text = a + '|' + label + \
                   '|' + str(aggregation[a].small_avg) + \
                   '|' + str(aggregation[a].medium_avg) + \
                   '|' + str(aggregation[a].large_avg) + \
                   '|' + str(aggregation[a].huge_avg) + \
                   '|' + str(aggregation[a].small_floor) + \
                   '|' + str(aggregation[a].medium_floor) + \
                   '|' + str(aggregation[a].large_floor) + \
                   '|' + str(aggregation[a].huge_floor) + \
                   '\n'
        file.write(text)
    file.flush()
    file.close()


def aggregate_rent(report, a, b, label, town = None):
    # Read the price collection
    client = ArangoClient(username='admin',password='F33dback34')
    db = client.database(name="dublin")
    prices_col = db.collection(name="property_rent")
    if town:
        query = "FOR d IN property_rent FILTER d.date >= '" + a + "' AND d.date <= '" + b + \
                "' AND LIKE(LOWER(d.town),LOWER('" + town + "')) RETURN d"
    else:
        query = "FOR d IN property_rent FILTER d.date >= '" + a + "' AND d.date <= '" + b + "' RETURN d"
    prices = db.aql.execute(query, ttl=7200)
    towns = db.collection(name="towns")
    towns.load()
    tlist = list()
    for t in towns:
        tlist.append(t['name'])
    a = prices.statistics()
    print("Retrieved %d properties" % a['scanned_index'] )

    # Prepare placeholder for the aggregation
    aggregation = dict()
    for t in tlist:
        aggregation[t] = RentRecord()

    # Process records
    i = 0
    started = datetime.datetime.now()
    for rec in prices:
        t = None
        if ("town" not in rec):
            t = extract_town( name = rec["name"], towns = tlist)
        else:
            t = rec["town"]
        if ( t == None ):
            print("No town is defined: %s" % rec)
            continue
        if ( t not in aggregation ):
            aggregation[t] = RentRecord()
        aggregation[t].process( rec = rec )
        i += 1
        rem = i/1000
        c = rem - math.floor(rem)
        if ( c==0 ):
            progress(started=started, i=i)
    progress(started=started, i=i)

    # Print the report
    file = open(report, 'a')
    for a in aggregation:
        if ( town and a != town ):
            continue
        aggregation[a].average()
        text = a + '|' + label
        idx = 1
        for i in aggregation[a].avg:
            while (idx < i):
                text += '|'
                idx += 1
            text += '|' + str(aggregation[a].avg[i])
            idx += 1
        text += '\n'
        file.write(text)
    file.flush()
    file.close()





parser = argparse.ArgumentParser()
parser.add_argument("--sale", action='store_true', help='Dwellings for sale')
parser.add_argument("--rent", action='store_true', help='Dwellings for rent')
parser.add_argument("--dates", nargs='+', help='Specify date range a > x < b (dates must be in YYYY-MM-DD format)')
parser.add_argument("--file", nargs='?', help='Report file name', default='aggregation_towns.log')
parser.add_argument("--label", nargs='?', help='Text label of the range, for example June2017')
parser.add_argument("--town", nargs='?', help='Name of the town i.e. Stepaside')
args = parser.parse_args()
if( args.sale ):
    aggregate_sale(report=args.file, a=args.dates[0], b=args.dates[1], label=args.label, town=args.town)
elif( args.rent ):
    aggregate_rent(report=args.file, a=args.dates[0], b=args.dates[1], label=args.label, town=args.town)
else:
    print("One of --sale or --rent aggegator modes must be used")

