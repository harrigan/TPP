#!/usr/bin/python3

import xml.etree.ElementTree as ET
from enum import Enum
from arango import ArangoClient

LATITUDE_BEGIN = 53.234407
LONGITUDE_BEGIN = -6.464235
LATITUDE_END = 53.463326
LONGITUDE_END = -6.041051
STEP = 0.002  # Approx 100 meters step
FILE_NAME = "map.xml"

class ItemType(Enum):
    undefined = 0
    shop = 1
    pub = 2
    hotel = 3
    school = 5
    creche = 6
    takeaway = 7
    restaurant = 8
    cinema = 9
    gym = 10
    post = 11
    museum = 12
    atm = 13
    police = 14
    firestation = 15
    college = 16
    leisure = 17
    theatre = 18
    public_transport = 19


class TransportType(Enum):
    undefined = 0
    dublin_bus = 1
    airport_coach = 2
    commuter_bus = 3
    intercity_bus = 4
    tram = 5
    train = 6


class ShopType(Enum):
    undefined = 0
    supermarket = 1
    convenience = 2
    clothes = 3
    beauty = 4
    diy = 5
    home = 6
    electronic = 7
    sport_tourism = 8
    hobby = 9
    media = 10
    kids = 11
    pets = 12
    laundry = 13
    motoring = 14
    fuel = 15
    bet = 16
    medical = 17
    charity = 18
    business = 19


class Item:
    name = ""
    lat = None
    lon = None
    type = None
    subtype = None
    website = None

    def print(self):
        print( self.name + "|" + str(self.lat) + "|" + str(self.lon) + "|" + str(self.type) + "|" + str(self.website))


def extract_shops(file_name):
    # Initialize the client for ArangoDB
    client = ArangoClient(username='admin',password='F33dback34')
    db = client.database(name="dublin")
    col = db.collection(name="map_provisioning")
    col.load()
    shop_types = col.get(key="shop_types")

    # Parse XML map
    tree = ET.parse(file_name)
    root = tree.getroot()
    for child in root:
        if ( child.tag=="node" ):
            item = Item()
            item.lat = child.attrib["lat"]
            item.lon = child.attrib["lon"]
            for node in child:
                if ( node.tag=="tag" ):
                    if ( "k" in node.attrib ):
                        if (node.attrib["k"]=="shop" and item.type==None):
                            item.type = ItemType.shop
                            value = node.attrib["v"]
                            v = value.lower()
                            try:
                                item.subtype = shop_types[v]
                            except KeyError as err:
                                item.subtype = "undefined"
                        if (node.attrib["k"]=="name" or node.attrib["k"]=="brand" or node.attrib["k"]=="operator"):
                            item.name = node.attrib["v"]
                        if (node.attrib["k"]=="amenity"):
                            if (node.attrib["v"]=="fuel"):
                                item.type = ItemType.shop
                                item.subtype = "fuel"
                            if (node.attrib["v"]=="pharmacy"):
                                item.type = ItemType.shop
                                item.subtype = "medical"
                            elif (node.attrib["v"]=="theatre"):
                                item.type = "theatre"
            if (item.type==ItemType.shop):
                item.print()


class MapItem:
    lat = None
    lon = None
    score = 0

    def print(self):
        print( str(self.lat) + "|" + str(self.lon) + "|" + str(self.score))


def process_item(item, area, col, db):
    query = 'FOR doc IN NEAR("grid_200m",' + str(item.lat) + ',' + str(item.lon) + ','+ str(area) +') RETURN doc'
    cursor = db.aql.execute(query)
    docs = cursor.batch()
    for doc in docs:
        price_ref = doc.get("price_ref")
        score = 0
        map_item = {'reference': price_ref, 'type': item.subtype}
        r = col.insert(map_item)


def public_transport(file_name):
    # Initialize the client for ArangoDB
    client = ArangoClient(username='admin',password='F33dback34')
    db = client.database(name="dublin")
    col = db.collection(name="transport")

    # Parse XML map
    tree = ET.parse(file_name)
    root = tree.getroot()
    for child in root:
        if ( child.tag=="node" ):
            item = Item()
            item.lat = child.attrib["lat"]
            item.lon = child.attrib["lon"]
            #Refactor to use a table instead of multiple IFs
            for node in child:
                if ( node.tag=="tag" ):
                    if ( "k" in node.attrib ):
                        v = (node.attrib["v"]).lower()
                        k = (node.attrib["k"]).lower()
                        if (k=="name"):
                            item.name = node.attrib["v"]
                        if (k=="bus"):
                            item.type = ItemType.public_transport
                            item.subtype = "city_bus"
                        if (k=="highway" and v=="bus_stop"):
                            item.type = ItemType.public_transport
                            item.subtype = "city_bus"
                        if (k=="railway" and v=="tram_stop"):
                            item.type = ItemType.public_transport
                            item.subtype = "tram"
                        if (k=="railway" and v=="station"):
                            item.type = ItemType.public_transport
                            item.subtype = "train"
                        if (k=="train"):
                            item.type = ItemType.public_transport
                            item.subtype = "train"
                        if (k=="operator"):
                            if (v=="dublin bus"):
                                item.type = ItemType.public_transport
                                item.subtype = "city_bus"
                            if (v == "aircoach"):
                                item.type = ItemType.public_transport
                                item.subtype = "airport_coach"
                            if (v == "citylink galway"):
                                item.type = ItemType.public_transport
                                item.subtype = "intercity_bus"
                            if (v == "swords express"):
                                item.type = ItemType.public_transport
                                item.subtype = "commuter_bus"
                            if (v == "bus éireann"):
                                item.type = ItemType.public_transport
                                item.subtype = "intercity_bus"
                            if (v == "finnegan bray"):
                                item.type = ItemType.public_transport
                                item.subtype = "commuter_bus"
                            if (v == "j.j. kavanagh &amp; sons"):
                                item.type = ItemType.public_transport
                                item.subtype = "intercity_bus"
                            if (v == "gobus"):
                                item.type = ItemType.public_transport
                                item.subtype = "intercity_bus"
                            if (v == "wexford bus"):
                                item.type = ItemType.public_transport
                                item.subtype = "intercity_bus"
                            if (v == "dublin coach group"):
                                item.type = ItemType.public_transport
                                item.subtype = "airport_coach"
                            if (v == "citylink"):
                                item.type = ItemType.public_transport
                                item.subtype = "intercity_bus"
            if (item.type==ItemType.public_transport):
                if (item.subtype=="city_bus"):
                    process_item( item=item, area=12, col=col, db=db)
                else:
                    process_item(item=item, area=25, col=col, db=db)
                print('.', end="", flush=True)


def school(file_name):
    # Initialize the client for ArangoDB
    client = ArangoClient(username='admin',password='F33dback34')
    db = client.database(name="dublin")
    col = db.collection(name="school")

    # Parse XML map
    tree = ET.parse(file_name)
    root = tree.getroot()
    for child in root:
        if ( child.tag=="node" ):
            item = Item()
            item.lat = child.attrib["lat"]
            item.lon = child.attrib["lon"]
            #Refactor to use a table instead of multiple IFs
            for node in child:
                if ( node.tag=="tag" ):
                    if ( "k" in node.attrib ):
                        v = (node.attrib["v"]).lower()
                        k = (node.attrib["k"]).lower()
                        if (k=="name"):
                            item.name = v
                        if (k=="amenity" and v=="school"):
                            item.type = ItemType.school
                        if (k=="website"):
                            item.website = v
            if (item.type==ItemType.school):
                item.print()

#extract_shops(FILE_NAME)
#public_transport(FILE_NAME)
school(FILE_NAME)
