#!/usr/bin/python3

from arango import ArangoClient
import argparse

class Transport:
    city_bus = False
    airport_coach = False
    commuter_bus = False
    intercity_bus = False
    tram = False
    train = False
    score = 0

    def score_calc(self):
        self.score = 0
        if (self.city_bus):
            self.score += 1
        if (self.airport_coach):
            self.score += 2
        if (self.commuter_bus):
            self.score += 1
        if (self.intercity_bus):
            self.score += 1
        if (self.tram):
            self.score += 2
        if (self.train):
            self.score += 2


def score_calc( trans ):
    # Sum all prices
    scores = dict()
    for t in trans:
        ref = t["reference"]
        type = t["type"]
        if ( ref not in scores ):
            scores[ref] = Transport()
        if ( type=="city_bus" ):
            scores[ref].city_bus = True
        elif ( type=="airport_coach" ):
            scores[ref].airport_coach = True
        elif ( type=="commuter_bus" ):
            scores[ref].commuter_bus = True
        elif ( type=="intercity_bus" ):
            scores[ref].intercity_bus = True
        elif ( type=="tram" ):
            scores[ref].tram = True
        elif ( type=="train" ):
            scores[ref].train = True

    # Average all prices
    for ref in scores:
        scores[ref].score_calc()
    return scores


def build_transport_map():
    # Read the price collection
    client = ArangoClient(username='admin', password='F33dback34')
    db = client.database(name="dublin")
    trans_col = db.collection(name="transport")
    trans_col.load()
    trans = trans_col.all()
    grid_col = db.collection(name="grid_200m")
    grid_col.load()
    grid = grid_col.all()

    # Sum all prices
    trans_map = score_calc( trans )

    # Iterate over grid
    for g in grid:
        if (g["price_ref"] in trans_map):
            print(g["price_ref"] + '|' + str(g["latitude"]) + '|' + str(g["longitude"]) + '|' + str(
                trans_map[g["price_ref"]].score))


build_transport_map()